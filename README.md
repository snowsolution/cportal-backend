#Backend app
- APIs routing
- Query model functions



#Code structure (from ./App)
- Models: contain Model classes
- Providers: providers class allow developer register or boostrap their class on application build time
- Repositories: contain Repository classes
- Http: contain APIs / controllers class
- Helper: contain Helper classes
