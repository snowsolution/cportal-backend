<?php

namespace App\Models\Source\Option;


class LeaveRequestStatus extends OptionAbstract
{

    public function toArray()
    {
        return [
            'Pending' => 'Pending',
            'Approved' => 'Approved',
            'Declined' => 'Declined',
        ];
    }
}
