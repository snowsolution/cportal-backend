<?php

namespace App\Models\Source\Option;

interface OptionInterface
{
    public function toOptionArray();

    public function toArray();
}