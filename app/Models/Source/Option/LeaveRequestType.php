<?php

namespace App\Models\Source\Option;


class LeaveRequestType extends OptionAbstract
{

    public function toArray()
    {
        return [
            'Sick / Personal Leave' => 'Sick / Personal Leave',
            'Parental Leave' => 'Parental Leave',
            'Leave without pay' => 'Leave without pay',
        ];
    }
}
