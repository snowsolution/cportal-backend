<?php

namespace App\Models\Source\Option;

abstract class OptionAbstract implements OptionInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $optionArray = [];
        foreach ($this->toArray() as $k => $v)
        {
            $optionArray[] = [
                'label' => $v,
                'value' => $k
            ];
        }
        return $optionArray;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [];
    }
}