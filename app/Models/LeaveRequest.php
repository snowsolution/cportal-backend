<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'type', 'from_date', 'to_date', 'reason', 'status', 'manager_comment'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    /**
     * Mutate the user name
     * @return mixed
     */
    public function getUserNameAttribute() {
        $user = $this->user;
        return $user->name;
    }
}
