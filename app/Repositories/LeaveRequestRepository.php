<?php

namespace App\Repositories;
use App\Models\LeaveRequest;
use Illuminate\Support\Facades\DB;

class LeaveRequestRepository extends AbstractRepository implements LeaveRequestRepositoryInterface
{

    public function getModel()
    {
        return \App\Models\LeaveRequest::class;
    }

    /**
     * Get all leave request
     * @param $userId
     * @return \Illuminate\Support\Collection|void
     */
    public function getAll($userId = false)
    {
        $query = DB::table('leave_requests')
            ->join('users', 'users.id', '=', 'leave_requests.user_id')
            ->select('leave_requests.*', 'users.name', 'users.email')->orderBy('id');
        if ($userId) {
            $query->where('users.id', '=', $userId);
        }
        return $query->get();
    }

    /**
     * Get leave request with a filter
     * @param $filters
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllByFilter($filters) {
        $collection = LeaveRequest::query();
        foreach ($filters as $filterKey => $filterValue) {
            $collection->where($filterKey, '=', $filterValue);
        }
        $collection->join('users', 'users.id', '=', 'leave_requests.user_id')->select([
            'users.name',
            'leave_requests.*'
        ]);
        return $collection->get();
    }
}
