<?php

namespace App\Repositories;
use App\Models\User;
use App\Models\UserPosition;
use App\Models\UserRole;
use Illuminate\Support\Facades\DB;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{

    public function getModel()
    {
        return \App\Models\User::class;
    }

    /**
     * Get all user
     * @return User[]|\Illuminate\Database\Eloquent\Collection|void
     */
    public function getAll()
    {
        $users = User::all();
        $users->map(function ($user, $id) {
            $user->role = $user->role;
            $user->position = $user->position;
            return $user;
        });
        return $users;
    }
}
