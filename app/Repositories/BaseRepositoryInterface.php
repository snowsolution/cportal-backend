<?php
namespace App\Repositories;

interface BaseRepositoryInterface
{
    public function get($id);

    public function save($object);

    public function getAll();

    public function delete($object);
}