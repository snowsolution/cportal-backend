<?php
namespace App\Helper\Api;
class ValidationErrorParser
{
    /**
     * Parse validation errors into readable array
     * @param \Illuminate\Support\MessageBag $errors
     * @return array
     */
    public static function toApiReadableErrors($errors) {
        $readableErrors = [];
        foreach ($errors->all() as $fieldError => $error) {
            $readableErrors[] = $error;
        }
        return $readableErrors;
    }
}
