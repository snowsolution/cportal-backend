<?php
namespace App\Helper;

use Illuminate\Database\Eloquent\Collection;

class OptionArray
{
    /**
     * Convert collection into option array
     * @param $collection
     * @param $keyField
     * @param $valueField
     * @return array
     */
    public static function collectionToOptionArray($collection, $keyField = 'id', $valueField ='name')
    {
        $optionArray = [];
        if ($collection instanceof Collection) {
            foreach ($collection as $item)
            {
                $optionArray[] = [
                    'value' => $item->$keyField,
                    'label' => $item->$valueField,
                ];
            }
        }
        return $optionArray;
    }

    /**
     * Convert collection to array with key as the primary key
     * @param $collection
     * @param $keyField
     * @param $valueField
     * @return array
     */
    public static function collectionToArray($collection, $keyField = 'id', $valueField ='name')
    {
        $optionArray = [];
        if ($collection instanceof Collection) {
            foreach ($collection as $item)
            {
                $optionArray[$item->$keyField] = $item->$valueField;
            }
        }
        return $optionArray;
    }
}
