<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Define singletons
     * @var string[]
     */
    public $singletons = [
        \App\Repositories\LeaveRequestRepositoryInterface::class => \App\Repositories\LeaveRequestRepository::class,
        \App\Repositories\UserRepositoryInterface::class => \App\Repositories\UserRepository::class,
    ];
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
