<?php

namespace App\Http\Controllers\Api;

use App\Helper\OptionArray;
use App\Http\Controllers\Controller;
use App\Models\UserRole;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $successStatus = 200;
    protected $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        return response()->json(
            $this->userRepository->getAll(),
            $this->successStatus
        );
    }
    /**
     * Login api
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $data = $request->all();
        if (Auth::attempt(
            [
                'email' => $data['email'],
                'password' => $data['password'],
            ]
        )) {
            $user = Auth::user();
            $token = $user->createToken('MyApp')->accessToken;
            $userData = $user->toArray();
            $userData['role'] = $user->role;
            $userData['position'] = $user->position;

            return response()->json(
                [
                    'status' => true,
                    'token' => $token,
                    'user' => $userData
                ],
                $this->successStatus
            );
        }
        return response()->json(
            [
                'status' => false,
                'error' => 'Email or password is incorrect'
            ], 401);
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'c_password' => 'required|same:password',
                'role_id' => 'required|exists:App\Models\UserRole,id',
                'position_id' => 'required|exists:App\Models\UserPosition,id'
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'error' => $validator->errors()
                ], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;

        return response()->json(
            [
                'success' => $success
            ],
            $this->successStatus
        );
    }

    /**
     * Details api
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        $user = Auth::user();

        return response()->json(
            [
                'user' => $user
            ],
            $this->successStatus
        );
    }

    /**
     * Set user role
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setRole(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'role_id' => 'required|exists:App\Models\UserRole,id',
                'user_id' => 'required|exists:App\Models\User,id',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'error' => $validator->errors()
                ], 401);
        }

        $postData = $request->all();
        $user = User::all()->where('id', $postData['user_id'])->first();
        if ($user) {
            $user->role_id = $postData['role_id'];
            $user->save();
            return response()->json(
                $user
            );
        } else {
            return response()->json(
                [
                    'error' => __('Resource does not exist')
                ], 401);
        }
    }

    /**
     * Return required assets for the resource form
     */
    public function getAssets()
    {
        return response()->json([
            'user_role' => OptionArray::collectionToArray(UserRole::all()),
        ]);
    }
}
