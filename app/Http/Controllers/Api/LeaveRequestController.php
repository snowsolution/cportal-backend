<?php

namespace App\Http\Controllers\Api;

use App\Helper\Api\ValidationErrorParser;
use App\Http\Controllers\Controller;
use App\Models\LeaveRequest;
use App\Models\Source\Option\LeaveRequestStatus;
use App\Models\Source\Option\LeaveRequestType;
use App\Repositories\LeaveRequestRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LeaveRequestController extends Controller
{
    protected $leaveRequestRepository;

    public $successStatus = 200;

    protected $fieldValidationRules;


    public function __construct(
        LeaveRequestRepositoryInterface $leaveRequestRepository
    )
    {
        $this->leaveRequestRepository = $leaveRequestRepository;
        $this->fieldValidationRules = [
            'user_id' => 'required|exists:App\Models\User,id',
            'type' => [
                'required',
                Rule::in(array_keys((new LeaveRequestType())->toArray()))
            ],
            'from_date' => 'required',
            'to_date' => 'required',
            'status' => [
                'required',
                Rule::in(array_keys((new LeaveRequestStatus())->toArray()))
            ],
        ];
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
        if (array_key_exists('user_id', $data)) {
            $userId = $data['user_id'];
            $result = $this->leaveRequestRepository->getAll($userId)->toArray();
        } else {
            $result = $this->leaveRequestRepository->getAll()->toArray();
        }

        return response()->json($result);
    }

    public function filter(Request $request)
    {
        $data = $request->all();
        if (count($data) > 0) {
            $result = $this->leaveRequestRepository->getAllByFilter($data)->toArray();
        } else {
            $result = $this->leaveRequestRepository->getAll()->toArray();
        }

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:App\Models\User,id',
                'type' => [
                    'required',
                    Rule::in(array_keys((new LeaveRequestType())->toArray()))
                ],
                'from_date' => 'required',
                'to_date' => 'required',
                'status' => [
                    'required',
                    Rule::in(array_keys((new LeaveRequestStatus())->toArray()))
                ],
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'error' => ValidationErrorParser::toApiReadableErrors($validator->errors())
                ], 401);
        }

        $postData = $request->all();
        $leaveRequest = LeaveRequest::create($postData);

        return response()->json(
            [
                'data' => $leaveRequest,
                'status' => true
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\LeaveRequest $leaveRequest
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveRequest $leaveRequest)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:App\Models\LeaveRequest,id',
                'user_id' => 'required|exists:App\Models\User,id',
                'type' => [
                    'required',
                    Rule::in(array_keys((new LeaveRequestType())->toArray()))
                ],
                'from_date' => 'required',
                'to_date' => 'required',
                'status' => [
                    'required',
                    Rule::in(array_keys((new LeaveRequestStatus())->toArray()))
                ],
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'error' => ValidationErrorParser::toApiReadableErrors($validator->errors())
                ], 401);
        }

        $postData = $request->all();
        $leaveRequest = LeaveRequest::all()->where('id', $postData['id'])->first();
        if ($leaveRequest) {
            $leaveRequest->fill($postData)->save();
            return response()->json(
                [
                    'data' => $leaveRequest,
                    'status' => true
                ]
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'error' => __('Resource does not exist')
                ], 401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePartial(Request $request)
    {
        $ruleToValidate = [
            'id' => 'required|exists:App\Models\LeaveRequest,id',
        ];
        foreach ($request->all() as $field => $value) {
            if (in_array($field, array_keys($this->fieldValidationRules))) {
                $ruleToValidate[$field] = $this->fieldValidationRules[$field];
            }
        }
        $validator = Validator::make($request->all(),
            $ruleToValidate
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'error' => ValidationErrorParser::toApiReadableErrors($validator->errors())
                ], 401);
        }

        $postData = $request->all();
        $leaveRequest = LeaveRequest::all()->where('id', $postData['id'])->first();
        if ($leaveRequest) {
            foreach ($postData as $field => $value) {
                $leaveRequest->$field = $value;
            }
            $leaveRequest->save();
            return response()->json(
                [
                    'data' => $leaveRequest,
                    'status' => true
                ]
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'error' => __('Resource does not exist')
                ], 401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:App\Models\LeaveRequest,id',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'error' => $validator->errors()
                ], 401);
        }

        $postData = $request->all();
        $leaveRequest = LeaveRequest::all()->where('id', $postData['id'])->first();
        if ($leaveRequest) {
            $leaveRequest->delete();
            return response()->json(
                ['status' => true]
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'error' => __('Resource does not exist')
                ], 401);
        }
    }

    /**
     * Return required assets for the resource form
     */
    public function getAssets()
    {
        return response()->json([
            'leave_request_type' => (new LeaveRequestType())->toArray(),
            'status' => (new LeaveRequestStatus())->toArray(),
        ]);
    }
}
