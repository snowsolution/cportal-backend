-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Dec 05, 2021 at 02:26 PM
-- Server version: 5.7.36
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_cportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_requests`
--

CREATE TABLE `leave_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `reason` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manager_comment` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leave_requests`
--

INSERT INTO `leave_requests` (`id`, `user_id`, `type`, `from_date`, `to_date`, `reason`, `status`, `manager_comment`, `created_at`, `updated_at`) VALUES
(5, 3, 'Sick / Personal Leave', '2021-12-04', '2021-12-05', 'Di choi cai nhe', 'Approved', 'Kidding me?', '2021-12-04 10:48:08', '2021-12-05 13:08:44'),
(6, 3, 'Leave without pay', '2021-12-16', '2021-12-22', 'Relax', 'Approved', 'Pick another day please', '2021-12-04 10:53:02', '2021-12-05 13:08:43'),
(7, 3, 'Parental Leave', '2021-12-29', '2021-12-30', 'Birthday', 'Approved', 'Stay safe', '2021-12-04 10:53:20', '2021-12-05 11:52:18'),
(8, 3, 'Sick / Personal Leave', '2021-11-29', '2021-11-30', 'Sick', 'Approved', 'Take care', '2021-12-04 10:53:34', '2021-12-05 13:08:42'),
(9, 4, 'Sick / Personal Leave', '2021-12-05', '2021-12-06', 'Going on a trip', 'Approved', NULL, '2021-12-05 10:47:56', '2021-12-05 13:08:39'),
(10, 4, 'Leave without pay', '2021-12-15', '2021-12-18', 'My wedding', 'Approved', NULL, '2021-12-05 10:48:34', '2021-12-05 13:08:40'),
(11, 4, 'Sick / Personal Leave', '2021-12-30', '2021-12-31', 'Health check', 'Approved', NULL, '2021-12-05 10:58:41', '2021-12-05 13:08:40'),
(12, 5, 'Sick / Personal Leave', '2021-12-13', '2021-12-15', 'Health check', 'Approved', NULL, '2021-12-05 11:00:40', '2021-12-05 13:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(10, '2021_11_15_144926_create_leave_requests_table', 1),
(11, '2021_11_15_154010_create_user_roles_table', 1),
(12, '2021_11_15_163547_create_user_positions_table', 1),
(13, '2021_11_15_164011_update_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('032d4da5deea21497fb9f4841d2bf366f8b06adcf022c46de78a04d2e448d2a457bed7183a96ab6b', 3, 3, 'MyApp', '[]', 0, '2021-12-05 13:19:21', '2021-12-05 13:19:21', '2022-12-05 13:19:21'),
('0338051ddff1f64ae0970af4b84c104ec56e2ec8ae6ec32fc745467c4b3fdba95f571cd06e73ccd8', 3, 1, 'MyApp', '[]', 0, '2021-11-26 10:14:46', '2021-11-26 10:14:46', '2022-11-26 10:14:46'),
('094fbbf44b7e4f4ceb3233945f8438647fd22a1e2bd5f587970c7b154d245a4615ed5086f8c9c09e', 3, 3, 'MyApp', '[]', 0, '2021-12-05 10:52:25', '2021-12-05 10:52:25', '2022-12-05 10:52:25'),
('104a221edfef27df849558438bdb55aff3eabba217eff04ac216629db505f72d7d7ce1fb46e8f5d3', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:51:44', '2021-12-03 07:51:44', '2022-12-03 07:51:44'),
('12de646af07ecaab2652e544efa235a0a0e97b25d80b80d4cb5f9506df2954b277625655a80aa4b8', 3, 1, 'MyApp', '[]', 0, '2021-12-03 16:08:55', '2021-12-03 16:08:55', '2022-12-03 16:08:55'),
('14786a4cf2c87e9790d7126604f2c8c5cf1155c8362ad82443b4111b2c5bcb8f7e828e3fecbe502a', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:50:42', '2021-12-03 07:50:42', '2022-12-03 07:50:42'),
('1664d8cd248a9def68efbec6af2f925a74e19015be9a018d10544242ecaae545f690005a3115554f', 3, 1, 'MyApp', '[]', 0, '2021-11-26 08:32:42', '2021-11-26 08:32:42', '2022-11-26 08:32:42'),
('1f0e95f5cc0273cf5e75a06559a883f4d680c791c1fec357666a3ce61872d00d0c259d10da500e09', 3, 1, 'MyApp', '[]', 0, '2021-12-03 19:05:20', '2021-12-03 19:05:20', '2022-12-03 19:05:20'),
('2039f437996136255a0172e099ad23b74fc2b2bdd2c7ed115e9dc67758ff60a0cf6263e166e06223', 3, 1, 'MyApp', '[]', 0, '2021-12-03 18:53:53', '2021-12-03 18:53:53', '2022-12-03 18:53:53'),
('219141791fbe9d978fe3206b812a2af1178d7ee90392dd6eb9d566e5f8aad4b21d1a6db39d57322f', 4, 1, 'MyApp', '[]', 0, '2021-11-26 10:14:00', '2021-11-26 10:14:00', '2022-11-26 10:14:00'),
('24c4e155a49aaf1e2cf57ef739c0548bfc71bc5d28e3690bc348a05b6575854deee8f80838f7f1e3', 5, 3, 'MyApp', '[]', 0, '2021-12-05 11:52:35', '2021-12-05 11:52:35', '2022-12-05 11:52:35'),
('26a231ee1d36d622fe86b160b344348108510467efb0e1825bfa061b7326e975ecebf1b4c18f5791', 3, 3, 'MyApp', '[]', 0, '2021-12-05 10:55:20', '2021-12-05 10:55:20', '2022-12-05 10:55:20'),
('2c1545014451f19fbfcbc6d869bbb80116dd0a2caae4d445c18aa498057ad00a49f29afab069cd62', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:13:46', '2021-12-05 11:13:46', '2022-12-05 11:13:46'),
('3446caad1ae12083f65ef9853d4f323a7333e003c84d75a3db4c5b2fedac637024c8f65f90497511', 3, 1, 'MyApp', '[]', 0, '2021-12-03 09:18:16', '2021-12-03 09:18:16', '2022-12-03 09:18:16'),
('34c3e73726e0dd2a53c9e56639776013fba4b258f5578a71486dc6809ac3115347eb73f6c0f1c214', 3, 1, 'MyApp', '[]', 0, '2021-12-03 08:30:05', '2021-12-03 08:30:05', '2022-12-03 08:30:05'),
('37e44d9d28fcc6ea5fed61a00f4ea21ecd533da91bb251bd78e4de1f29e51dd78b7d2afce203752f', 3, 3, 'MyApp', '[]', 0, '2021-12-05 08:28:40', '2021-12-05 08:28:40', '2022-12-05 08:28:40'),
('402371d0fe2f9af992ed2daf823843558e6a54ac1592591cbba997b5fb66bc8c1c9f1e52ba9d0831', 3, 1, 'MyApp', '[]', 0, '2021-12-03 09:37:43', '2021-12-03 09:37:43', '2022-12-03 09:37:43'),
('46a5e85fd2a0a256d65996f0f3a47414ee8d20271c6ba53ab82c3622f491787edf836e8cd040e819', 4, 3, 'MyApp', '[]', 0, '2021-12-05 10:38:05', '2021-12-05 10:38:05', '2022-12-05 10:38:05'),
('4abd74dba6f9c3d9f851a05bfe7eac456d5fd37a50815a549887918c408e7903c2191e5e83189888', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:43:37', '2021-12-03 07:43:37', '2022-12-03 07:43:37'),
('4accae848d565de2755e74d61be8e31515f4e709989597c4a0cb35122d68528c4889833baf488ccc', 3, 3, 'MyApp', '[]', 0, '2021-12-05 14:12:12', '2021-12-05 14:12:12', '2022-12-05 14:12:12'),
('4ad76c295dbbd8071d9289bf4700de5326f52c5398a8bb94d372ee32663f39b82eb5f84f3350dfc9', 3, 1, 'MyApp', '[]', 0, '2021-12-03 18:48:59', '2021-12-03 18:48:59', '2022-12-03 18:48:59'),
('4cf5829195dcac65c8f31e7a9c610ff2e1dfe9204e204ca9ea75962da75673b2abfd5546299db5aa', 3, 3, 'MyApp', '[]', 0, '2021-12-05 08:52:48', '2021-12-05 08:52:48', '2022-12-05 08:52:48'),
('4da4ed87f747ccf3a322fe6e8af3f331936e98d5fc86d217118d151d1e90e2a59a8e4e6989f5ed85', 3, 3, 'MyApp', '[]', 0, '2021-12-05 10:34:43', '2021-12-05 10:34:43', '2022-12-05 10:34:43'),
('4e17de4b0683798a606c8df0e03476140a58c924fca64620ce9c214d69b534ee94e97dbd007ef730', 3, 3, 'MyApp', '[]', 0, '2021-12-05 13:19:36', '2021-12-05 13:19:36', '2022-12-05 13:19:36'),
('50c1068cd8f1bfdc1f070d4d3e2ca52f128067a0aaefa03eaa03f91bf487b426c939689a510b0307', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:45:30', '2021-12-03 07:45:30', '2022-12-03 07:45:30'),
('534fa921e345c4f2180322fbbbd60efdd0e752ccc5b46d4b3c5439b614e77a448ce74e798ac60765', 3, 3, 'MyApp', '[]', 0, '2021-12-05 14:12:52', '2021-12-05 14:12:52', '2022-12-05 14:12:52'),
('5540ea3e992f4d9a37f8cc82099450f2598ff5b7a6c6432fe0e2f2d06d9c204ba13b24e53fa50725', 3, 1, 'MyApp', '[]', 0, '2021-12-03 19:05:13', '2021-12-03 19:05:13', '2022-12-03 19:05:13'),
('57e53fed72972bd0f3bd9214efa81d62e274808d273f70f7be4192d05806141cdc570d62a865bf39', 3, 3, 'MyApp', '[]', 0, '2021-12-05 10:53:54', '2021-12-05 10:53:54', '2022-12-05 10:53:54'),
('5abfb4a9705fcd55ea26e55af8cf87e32fc59907ea0f51617ff5852e171dbf8605f1299615d20e45', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:41:11', '2021-12-03 07:41:11', '2022-12-03 07:41:11'),
('5c2a89b6ae86e042dbfb8a32ebad23737fe357831c52e1842a6ea86414a7a6fae9d9183e7bc0b9ab', 4, 3, 'MyApp', '[]', 0, '2021-12-05 10:51:34', '2021-12-05 10:51:34', '2022-12-05 10:51:34'),
('61b517c630489b9a2b8df11b70fd0a701daa00d7cbd7d87ff45f57139825db78bf89bd607737e402', 3, 1, 'MyApp', '[]', 0, '2021-11-26 08:34:20', '2021-11-26 08:34:20', '2022-11-26 08:34:20'),
('627b8e36c678e0abc3c7522cc2e3c50a15ca446616ef1aa806f3f6e6ad26272eed6579b3e98e2ee5', 5, 3, 'MyApp', '[]', 0, '2021-12-05 11:00:14', '2021-12-05 11:00:14', '2022-12-05 11:00:14'),
('641d4420d386d6d8d7b0c7abfb88587850281809d03ee40de5f0d9c5bb1e15fc8a808453ddcf0bfa', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:40:00', '2021-12-03 07:40:00', '2022-12-03 07:40:00'),
('65de0a09a5f4a9134a1db6bf8d05443f7ca97a3c6f40f48f90b2d185aef6c9025332d619160fac23', 5, 1, 'MyApp', '[]', 0, '2021-11-26 09:11:59', '2021-11-26 09:11:59', '2022-11-26 09:11:59'),
('6a1c9baf7907fb88bece2874b4c047858f1c862f5bb66d7f17e9a663ac3ec53c5a8068f2c9a5ff62', 4, 1, 'MyApp', '[]', 0, '2021-11-26 09:11:48', '2021-11-26 09:11:48', '2022-11-26 09:11:48'),
('6a6e3cc13ecae7d02eeed31624df94f93001e4250a34541f7c81ee3ab8ef7c4844b2ade0b39241ea', 5, 3, 'MyApp', '[]', 0, '2021-12-05 11:13:29', '2021-12-05 11:13:29', '2022-12-05 11:13:29'),
('6a824ba60f503c3a26b1729ffc7ad60444fff42bbeae48a3ee01392ff7e0c1d1d08c382680a58a6d', 6, 1, 'MyApp', '[]', 0, '2021-11-26 09:12:09', '2021-11-26 09:12:09', '2022-11-26 09:12:09'),
('6c26780d772065b789915201cbd49c271c8396ac9e700bbc684ea037fe56fb5d807b38c53165e3b3', 3, 3, 'MyApp', '[]', 0, '2021-12-05 10:55:13', '2021-12-05 10:55:13', '2022-12-05 10:55:13'),
('702a76267ff967f03b58877e091906fc751d9f8ca7351497c45dba4a428b7a79dc689fbbaa1a9118', 3, 1, 'MyApp', '[]', 0, '2021-12-03 08:24:41', '2021-12-03 08:24:41', '2022-12-03 08:24:41'),
('702f5107a7fb18fe33c0acd5e78bfb8083442798a2c4d46b429f948367b9e7d74530398b957f71e6', 3, 1, 'MyApp', '[]', 0, '2021-12-03 09:37:30', '2021-12-03 09:37:30', '2022-12-03 09:37:30'),
('708dd705bd8159dbd378f02f8dfa0bde9a503003ca08da69433bf6b781c1d9414636f2a67be7923a', 3, 1, 'MyApp', '[]', 0, '2021-12-04 07:05:23', '2021-12-04 07:05:23', '2022-12-04 07:05:23'),
('72511f1703db21c32ae52e81553359a626f1ce2e904d28e00011397c2215f2517c0bbf73e33bdef7', 9, 1, 'MyApp', '[]', 0, '2021-11-26 09:12:39', '2021-11-26 09:12:39', '2022-11-26 09:12:39'),
('72b4ba3cc108b4217b493e091f5ea343ead1568ee44cf9b9de63780e759f016c5cca9e0059fa81ee', 3, 3, 'MyApp', '[]', 0, '2021-12-05 10:53:14', '2021-12-05 10:53:14', '2022-12-05 10:53:14'),
('73ef1b8f320888c9c37deb6f84e612b38f2d1f2f8ae9429958cd088f01085a3b5795f298f58a8901', 3, 1, 'MyApp', '[]', 0, '2021-12-03 16:08:58', '2021-12-03 16:08:58', '2022-12-03 16:08:58'),
('7b8f2339330cff8de5b9a4bdecfbf70a0e753b639df47b152e02be2ebaa76bb0ef3895efe69a8150', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:59:18', '2021-12-03 07:59:18', '2022-12-03 07:59:18'),
('8003ec92ccc13d008fb6fbccc92632a65b0fedf43c16dfb990767b836ae0a0ab74baa5e4e416903a', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:32:36', '2021-12-05 11:32:36', '2022-12-05 11:32:36'),
('80b1d0a64f02340e58bfa9d560253756df97ac54c41b4d336a6ddc30ad7736a80e5e2a04329285bc', 4, 3, 'MyApp', '[]', 0, '2021-12-05 10:49:55', '2021-12-05 10:49:55', '2022-12-05 10:49:55'),
('80c10863fb9a04b23e18be85d25e53ec301e13d80ce52738782250bcfa4508ebe7b54674348db66b', 3, 1, 'MyApp', '[]', 0, '2021-11-26 10:12:46', '2021-11-26 10:12:46', '2022-11-26 10:12:46'),
('83d3b31d489d4c1d3cfa0c4da9e8da847b21d23ecc51e87e1c5797daf0887678ea3398f3ee1edf4e', 3, 1, 'MyApp', '[]', 0, '2021-12-03 08:09:20', '2021-12-03 08:09:20', '2022-12-03 08:09:20'),
('84b300403197960bfcc907bdeb89d1a046610364bc4a22e96421b20d5e780831c58a6646ac40570e', 3, 1, 'MyApp', '[]', 0, '2021-12-03 08:05:48', '2021-12-03 08:05:48', '2022-12-03 08:05:48'),
('8525868b438603df5172f1c55718dec8427190ec29afc8e03884aec88155f0c002ada08579cf521d', 5, 3, 'MyApp', '[]', 0, '2021-12-05 11:03:30', '2021-12-05 11:03:30', '2022-12-05 11:03:30'),
('866bc766e5e128b90885ce1ebe9778feb6fd18bad27e862636a6bf33ce03b1e8dc36f5a478e0a4ad', 3, 1, 'MyApp', '[]', 0, '2021-12-03 09:37:09', '2021-12-03 09:37:09', '2022-12-03 09:37:09'),
('88b2c635a6e43e91c601d7db5c2ed7ed06c20ceb727f85c841d7ed16ae1b0394a898baedbe870aec', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:42:13', '2021-12-03 07:42:13', '2022-12-03 07:42:13'),
('8c2f328c7ddad387682baa987e501aab2bdb1a34b56f1375a3c14f0dfefd96dc65a3aadce55e22d5', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:13:19', '2021-12-05 11:13:19', '2022-12-05 11:13:19'),
('949df3771380ba3a97aebdbfd9275035db89205440a8614ec16843f64ca449357b58754e4cbdaa92', 3, 3, 'MyApp', '[]', 0, '2021-12-05 10:57:15', '2021-12-05 10:57:15', '2022-12-05 10:57:15'),
('992af289adf417845f8df3bc1e7f9131684106799393c93d1e07903f89b5ce822e63b88ca810100a', 3, 1, 'MyApp', '[]', 0, '2021-12-03 16:54:46', '2021-12-03 16:54:46', '2022-12-03 16:54:46'),
('9951a76180540c199a32a1ee40fda7a37600c51695edad6a9bd24e3b4f1ce4e8ea2f4e9c43ef7d9e', 4, 3, 'MyApp', '[]', 0, '2021-12-05 10:57:21', '2021-12-05 10:57:21', '2022-12-05 10:57:21'),
('9f383fd4ef7c0ab7967fb7f2a50aac50c51a060f8d7851124148fb0c110d61bccac0e32947940870', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:01:45', '2021-12-05 11:01:45', '2022-12-05 11:01:45'),
('a7beeb1954e10411ccec2430c83fe778df62a496d41f176a44c6bdf3024d5cb655f76701d2bda86d', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:25:51', '2021-12-05 11:25:51', '2022-12-05 11:25:51'),
('ab988c6ea2b54c494eb2f54837eada43746ce6794bd73d0e593b1fef35dce574c709cb33bc8c87c4', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:39:35', '2021-12-03 07:39:35', '2022-12-03 07:39:35'),
('ac36ce384dcc4eae88c74c5e537ce0a6cc492b9e491d5b9de23467599178dcf7f8a254360ec698b1', 3, 1, 'MyApp', '[]', 0, '2021-12-03 19:03:18', '2021-12-03 19:03:18', '2022-12-03 19:03:18'),
('ac67a90c533ff000425553f5c163d8ea5904edf0c611de84c31468c21f0427a4c38f8f8014b501cd', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:44:19', '2021-12-03 07:44:19', '2022-12-03 07:44:19'),
('ac732662749874fe9094324c9c2b410701180e01aa0d474bdb5763882d4071a4655136864ef87542', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:44:00', '2021-12-03 07:44:00', '2022-12-03 07:44:00'),
('afae256347f2eda95e5426555e7bf8425230aa4ff13574109311a7a29fae4c0ce8c43cab3fdab721', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:40:19', '2021-12-03 07:40:19', '2022-12-03 07:40:19'),
('aff5c47876901e04d24b5cc9124b3b5e843505a9a4f490fab237ddad3096084f69016ea1978f5f5e', 3, 3, 'MyApp', '[]', 0, '2021-12-05 08:33:53', '2021-12-05 08:33:53', '2022-12-05 08:33:53'),
('b0ca954fafe75d5421a2c3da34af12d82161672f1003f76ac6581c4e659ebaceee376e457e8d7ce6', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:33:23', '2021-12-05 11:33:23', '2022-12-05 11:33:23'),
('b2b938e4b7aa8c3d337fb0122bd73eaf3b334f3f5bf3671208d686c8dec418ca000c6ac891b2c433', 3, 1, 'MyApp', '[]', 0, '2021-12-03 16:54:53', '2021-12-03 16:54:53', '2022-12-03 16:54:53'),
('b3a12e2285a74c6b70b4d725901dd799b0c87ad041b0fdda235f2e79af5387386094f5556f8077d1', 3, 1, 'MyApp', '[]', 0, '2021-12-03 21:10:43', '2021-12-03 21:10:43', '2022-12-03 21:10:43'),
('b75b921ab0572c67c738e00d5cd81c07a34a71770f367fdbd026a32e248e2eefeaf36856251bc6a9', 3, 1, 'MyApp', '[]', 0, '2021-11-26 08:34:06', '2021-11-26 08:34:06', '2022-11-26 08:34:06'),
('b883a6faa0875966b476f2ea332489d47308a16702e7366a61ea8851fcef84c8ca39658278141784', 3, 1, 'MyApp', '[]', 0, '2021-11-26 08:14:37', '2021-11-26 08:14:37', '2022-11-26 08:14:37'),
('b8f05666e0439197c88d43541ade691be7478ce98ec1bc812504ddd110ab92abde14ad525aa4e687', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:59:36', '2021-12-03 07:59:36', '2022-12-03 07:59:36'),
('b9b6c51d7169a54fe4473bcffbee11d0500cfd8fba4b2f3029f6db45e48122cf7f205c448776bd67', 3, 3, 'MyApp', '[]', 0, '2021-12-05 10:49:40', '2021-12-05 10:49:40', '2022-12-05 10:49:40'),
('baff77b876ef72cf7e5e3b358f526eb2da08c85008d6ce7d6f85fd40e5733b8bd21a09a036657864', 3, 1, 'MyApp', '[]', 0, '2021-12-01 15:15:46', '2021-12-01 15:15:46', '2022-12-01 15:15:46'),
('bcb064e22d32c3169ff7edc0f54771242c1ed352f0da3e4dcf4ec8d4ac20724b6d8ec0d7aab982bb', 3, 1, 'MyApp', '[]', 0, '2021-12-03 19:13:55', '2021-12-03 19:13:55', '2022-12-03 19:13:55'),
('be90e5a3f172c8fee9f930ccdb2769973d05eaeb96f17667391b02c68fe12d39092190a150c0705d', 3, 1, 'MyApp', '[]', 0, '2021-11-26 08:40:46', '2021-11-26 08:40:46', '2022-11-26 08:40:46'),
('bf08c97d9d813c89be56f4e7dd397efd2c09e3b1111d00a75fbd1e269104bfd173c1cb5e9c414a0a', 3, 1, 'MyApp', '[]', 0, '2021-12-03 08:10:05', '2021-12-03 08:10:05', '2022-12-03 08:10:05'),
('c014823ae7b033097d42f353c637a2b54740eb389eb7eadd83a075ab44528f10a9bed7a92a86f6b9', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:33:51', '2021-12-05 11:33:51', '2022-12-05 11:33:51'),
('c3950c0159683bbe9fe79cdea3826117fa0d746a648c1b108852ffde03838fe66e5609f6170943e7', 3, 3, 'MyApp', '[]', 0, '2021-12-05 14:14:02', '2021-12-05 14:14:02', '2022-12-05 14:14:02'),
('c96436e55bd3ef4d2506aec4702158b61bfba6df9b0de3d4ef0a72a0d475ab039186d8943903ab1a', 10, 1, 'MyApp', '[]', 0, '2021-11-26 09:12:46', '2021-11-26 09:12:46', '2022-11-26 09:12:46'),
('cc85d62c6aa887409b83d2290aded6b37167f2b2c8b94be5d750ac9bfdfbdbacaf8d46e0b0e1ce3f', 4, 3, 'MyApp', '[]', 0, '2021-12-05 10:55:47', '2021-12-05 10:55:47', '2022-12-05 10:55:47'),
('cfe63d7c81453a0f26c2f27f209f320a0396acf20e13b2c3acf4b6aa157494fe83d7101ca3ee1e0e', 8, 1, 'MyApp', '[]', 0, '2021-11-26 09:12:28', '2021-11-26 09:12:28', '2022-11-26 09:12:28'),
('d159893a7912c7d06bd8f3ae21c40c0ec1cd1f1beb6e0c4327630f1f9f0d0bb09ee7555319e4f2e1', 3, 1, 'MyApp', '[]', 0, '2021-12-03 07:51:58', '2021-12-03 07:51:58', '2022-12-03 07:51:58'),
('d2a9b74fc112d98272f8b5d32050fb13c3c5399effb454483c9d9cd8d38dccc4ba72ca8d7a8b31d1', 3, 1, 'MyApp', '[]', 0, '2021-12-03 19:14:20', '2021-12-03 19:14:20', '2022-12-03 19:14:20'),
('d35de95283a14b919e4c54998488cd6408e1fb6bc906bf8d291630d6c7db25dd2a4beafa4c1d37b6', 3, 1, 'MyApp', '[]', 0, '2021-11-26 08:15:06', '2021-11-26 08:15:06', '2022-11-26 08:15:06'),
('d4ecb5e94601d1be5691927d24b5944e3ec40cf2f8302329ee4b6caca249c95ddaf61d91b6d68071', 3, 3, 'MyApp', '[]', 0, '2021-12-05 14:19:21', '2021-12-05 14:19:21', '2022-12-05 14:19:21'),
('d534c55c1234ceacd8498521fc190e99efa9865c274de8d8677b11ddf558966af9cfff7eb81c45fb', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:55:15', '2021-12-05 11:55:15', '2022-12-05 11:55:15'),
('d5b6af66ae13c1d9b8b31292fca6d084cd88a8230b38d67d631854530c30bd28d240a5af86fafbe7', 3, 1, 'MyApp', '[]', 0, '2021-12-03 16:54:49', '2021-12-03 16:54:49', '2022-12-03 16:54:49'),
('d9c8577331b5ec1fed5c29d6c55a502e2b3af09a99a3b1ed94a8e632381b148cd134fac23d0a022f', 3, 1, 'MyApp', '[]', 0, '2021-12-03 19:01:30', '2021-12-03 19:01:30', '2022-12-03 19:01:30'),
('e5b088f018eb7a162d58a33bce323b6a009c35c9664a3ea7204345f5ef75ca697c9e0d49fa1d76b8', 3, 3, 'MyApp', '[]', 0, '2021-12-05 14:13:06', '2021-12-05 14:13:06', '2022-12-05 14:13:06'),
('e811f435cdfc77be15cad2fd0ff7b2f7f7c047f3df89f2d8a492d7b23f21feb21f67c60b20f07c52', 4, 3, 'MyApp', '[]', 0, '2021-12-05 10:55:31', '2021-12-05 10:55:31', '2022-12-05 10:55:31'),
('ea5ae33e3cad3530fddf104fa5b54067d4677dcc7e4ea4c7d8a03a87f9f62c4219bc3c77f7020768', 3, 1, 'MyApp', '[]', 0, '2021-11-26 10:11:59', '2021-11-26 10:11:59', '2022-11-26 10:11:59'),
('ef1319346242f70362bb12db2af97e01b7d4d56e87179381753ad50d6040e50b2340f3986d304ef7', 3, 1, 'MyApp', '[]', 0, '2021-12-03 08:18:05', '2021-12-03 08:18:05', '2022-12-03 08:18:05'),
('f23d02c08c56043e1bf7188fc3d26443aa385c942b1356aff6cf262b6ac16620e461d4fa14993ec5', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:13:13', '2021-12-05 11:13:13', '2022-12-05 11:13:13'),
('f313e53c1d28d3bceb0ce712b004ff0515d2d70866d7fb6b54395590fec009383a09769b171809ef', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:32:26', '2021-12-05 11:32:26', '2022-12-05 11:32:26'),
('f42973b0a4037538c0a464ca8d8e1f335bc55174c70c46cc13f6d458fcd0d68b2d50060d9d78861c', 3, 3, 'MyApp', '[]', 0, '2021-12-05 11:33:38', '2021-12-05 11:33:38', '2022-12-05 11:33:38'),
('fa0f0b6985c80c639e1bdabe3b51c5402e029a67c3981f762445ad3f6109c6d0acc84f744270fbed', 3, 1, 'MyApp', '[]', 0, '2021-12-03 20:23:23', '2021-12-03 20:23:23', '2022-12-03 20:23:23'),
('fb8f6942ae8fece48600bbda967d1f37fe2768af2ea5efb2d07c323b8cb106dd345e94fe655639a3', 3, 3, 'MyApp', '[]', 0, '2021-12-05 10:53:03', '2021-12-05 10:53:03', '2022-12-05 10:53:03'),
('ff4148b7e58fcb8069bef96540cb18931246bd12cc9284c9d249741a095c2e45db72e6671be7990b', 7, 1, 'MyApp', '[]', 0, '2021-11-26 09:12:16', '2021-11-26 09:12:16', '2022-11-26 09:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'FPjGkeIhwRtTAMo48zpRgYpaEDjkwnfauhTM9lMU', NULL, 'http://localhost', 1, 0, 0, '2021-11-26 08:14:20', '2021-11-26 08:14:20'),
(2, NULL, 'Laravel Password Grant Client', 'da5q9fYzTRX2tgdPSfHtvBmb9ePSU43e4pFtQJWS', 'users', 'http://localhost', 0, 1, 0, '2021-11-26 08:14:20', '2021-11-26 08:14:20'),
(3, NULL, 'Laravel Personal Access Client', 'kGKYgzxqnqxG8ytXQNlYfFbA5PuQfKBzW9BTl7jG', NULL, 'http://localhost', 1, 0, 0, '2021-12-05 08:28:02', '2021-12-05 08:28:02'),
(4, NULL, 'Laravel Password Grant Client', 't0r8rnjVJZMFbuNqRYGxnKqPShfAuItABChNdush', 'users', 'http://localhost', 0, 1, 0, '2021-12-05 08:28:02', '2021-12-05 08:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-11-26 08:14:20', '2021-11-26 08:14:20'),
(2, 3, '2021-12-05 08:28:02', '2021-12-05 08:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `position_id` bigint(20) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`, `position_id`) VALUES
(3, 'Phuc Nguyen', 'phucnguyen.snow@gmail.com', NULL, '$2y$10$euNQo8Hg.qgU/c4w5UuW3.Six7JAGq27Jfv9yi19eU.gdS.Wfvseq', NULL, '2021-11-26 08:14:37', '2021-11-26 08:14:37', 1, 1),
(4, 'Mark', 'mark@gmail.com', NULL, '$2y$10$UB8kqKRknZdqPRMuw6UfUOecsQC/DSMD1DfUtLECIs18.QstMJKfq', NULL, '2021-11-26 09:11:48', '2021-11-26 09:11:48', 2, 2),
(5, 'Jake', 'jake@gmail.com', NULL, '$2y$10$vL/LC4HdPHpoStPxITxmI.1Q20HMZlCY6XCIpI8zJyCYtfcQSxZ12', NULL, '2021-11-26 09:11:59', '2021-11-26 09:11:59', 2, 2),
(6, 'Kylie', 'kylie@gmail.com', NULL, '$2y$10$Z/2gJAns15IM5v5aLeNsPOWUdvSqGOCyoi8Q.chT6.0zHsQLK4vgS', NULL, '2021-11-26 09:12:09', '2021-11-26 09:12:09', 2, 2),
(7, 'Vince', 'vince@gmail.com', NULL, '$2y$10$/9glQfU5SMiJ7bA7ZPa3QeL6jRfeY60y1Ud64cegS6Io4.4iieIjq', NULL, '2021-11-26 09:12:16', '2021-11-26 09:12:16', 2, 2),
(8, 'Ostra', 'ostra@gmail.com', NULL, '$2y$10$RwBf38MxHHIKk1rlVqKYz.4CVCX7WDNqC7KjQQuq5RqvJS6..HKqa', NULL, '2021-11-26 09:12:28', '2021-11-26 09:12:28', 2, 2),
(9, 'Phil', 'phil@gmail.com', NULL, '$2y$10$wlYG1JWXvIQAgqmDg1OWk.e71eI.d632B.CsnuOF.//SZYARMyvMe', NULL, '2021-11-26 09:12:39', '2021-11-26 09:12:39', 2, 2),
(10, 'Robb', 'robb@gmail.com', NULL, '$2y$10$Spyta8.XN3bL77QQaqvV7exMZ5hB.lRaUjMDrhH4BWMgdXkGRjcYy', NULL, '2021-11-26 09:12:46', '2021-11-26 09:12:46', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_positions`
--

CREATE TABLE `user_positions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_positions`
--

INSERT INTO `user_positions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Director', NULL, NULL),
(2, 'Manager', NULL, NULL),
(3, 'Developer', NULL, NULL),
(4, 'QC', NULL, NULL),
(5, 'Sales Executive', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Manager', NULL, NULL),
(2, 'Employee', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `leave_requests`
--
ALTER TABLE `leave_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leave_requests_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`),
  ADD KEY `users_position_id_foreign` (`position_id`);

--
-- Indexes for table `user_positions`
--
ALTER TABLE `user_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_requests`
--
ALTER TABLE `leave_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_positions`
--
ALTER TABLE `user_positions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `leave_requests`
--
ALTER TABLE `leave_requests`
  ADD CONSTRAINT `leave_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_position_id_foreign` FOREIGN KEY (`position_id`) REFERENCES `user_positions` (`id`),
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
