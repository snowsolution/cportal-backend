-- MySQL dump 10.13  Distrib 5.7.36, for Linux (x86_64)
--
-- Host: localhost    Database: laravel_portal
-- ------------------------------------------------------
-- Server version	5.7.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leave_requests`
--

DROP TABLE IF EXISTS `leave_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leave_requests` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `reason` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manager_comment` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `leave_requests_user_id_foreign` (`user_id`),
  CONSTRAINT `leave_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leave_requests`
--

LOCK TABLES `leave_requests` WRITE;
/*!40000 ALTER TABLE `leave_requests` DISABLE KEYS */;
INSERT INTO `leave_requests` VALUES (5,3,'Sick / Personal Leave','2021-12-04','2021-12-04','Nghi xiu','Declined',NULL,'2021-12-04 10:48:08','2021-12-04 11:57:28'),(6,3,'Leave without pay','2021-12-16','2021-12-22','Relax','Approved',NULL,'2021-12-04 10:53:02','2021-12-04 11:57:23'),(7,3,'Parental Leave','2021-12-29','2021-12-30','Birthday','Declined',NULL,'2021-12-04 10:53:20','2021-12-04 11:57:28'),(8,3,'Sick / Personal Leave','2021-11-29','2021-11-30','Sick','Approved',NULL,'2021-12-04 10:53:34','2021-12-04 11:57:21');
/*!40000 ALTER TABLE `leave_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2019_08_19_000000_create_failed_jobs_table',1),(9,'2019_12_14_000001_create_personal_access_tokens_table',1),(10,'2021_11_15_144926_create_leave_requests_table',1),(11,'2021_11_15_154010_create_user_roles_table',1),(12,'2021_11_15_163547_create_user_positions_table',1),(13,'2021_11_15_164011_update_users_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('0338051ddff1f64ae0970af4b84c104ec56e2ec8ae6ec32fc745467c4b3fdba95f571cd06e73ccd8',3,1,'MyApp','[]',0,'2021-11-26 10:14:46','2021-11-26 10:14:46','2022-11-26 10:14:46'),('104a221edfef27df849558438bdb55aff3eabba217eff04ac216629db505f72d7d7ce1fb46e8f5d3',3,1,'MyApp','[]',0,'2021-12-03 07:51:44','2021-12-03 07:51:44','2022-12-03 07:51:44'),('12de646af07ecaab2652e544efa235a0a0e97b25d80b80d4cb5f9506df2954b277625655a80aa4b8',3,1,'MyApp','[]',0,'2021-12-03 16:08:55','2021-12-03 16:08:55','2022-12-03 16:08:55'),('14786a4cf2c87e9790d7126604f2c8c5cf1155c8362ad82443b4111b2c5bcb8f7e828e3fecbe502a',3,1,'MyApp','[]',0,'2021-12-03 07:50:42','2021-12-03 07:50:42','2022-12-03 07:50:42'),('1664d8cd248a9def68efbec6af2f925a74e19015be9a018d10544242ecaae545f690005a3115554f',3,1,'MyApp','[]',0,'2021-11-26 08:32:42','2021-11-26 08:32:42','2022-11-26 08:32:42'),('1f0e95f5cc0273cf5e75a06559a883f4d680c791c1fec357666a3ce61872d00d0c259d10da500e09',3,1,'MyApp','[]',0,'2021-12-03 19:05:20','2021-12-03 19:05:20','2022-12-03 19:05:20'),('2039f437996136255a0172e099ad23b74fc2b2bdd2c7ed115e9dc67758ff60a0cf6263e166e06223',3,1,'MyApp','[]',0,'2021-12-03 18:53:53','2021-12-03 18:53:53','2022-12-03 18:53:53'),('219141791fbe9d978fe3206b812a2af1178d7ee90392dd6eb9d566e5f8aad4b21d1a6db39d57322f',4,1,'MyApp','[]',0,'2021-11-26 10:14:00','2021-11-26 10:14:00','2022-11-26 10:14:00'),('3446caad1ae12083f65ef9853d4f323a7333e003c84d75a3db4c5b2fedac637024c8f65f90497511',3,1,'MyApp','[]',0,'2021-12-03 09:18:16','2021-12-03 09:18:16','2022-12-03 09:18:16'),('34c3e73726e0dd2a53c9e56639776013fba4b258f5578a71486dc6809ac3115347eb73f6c0f1c214',3,1,'MyApp','[]',0,'2021-12-03 08:30:05','2021-12-03 08:30:05','2022-12-03 08:30:05'),('402371d0fe2f9af992ed2daf823843558e6a54ac1592591cbba997b5fb66bc8c1c9f1e52ba9d0831',3,1,'MyApp','[]',0,'2021-12-03 09:37:43','2021-12-03 09:37:43','2022-12-03 09:37:43'),('4abd74dba6f9c3d9f851a05bfe7eac456d5fd37a50815a549887918c408e7903c2191e5e83189888',3,1,'MyApp','[]',0,'2021-12-03 07:43:37','2021-12-03 07:43:37','2022-12-03 07:43:37'),('4ad76c295dbbd8071d9289bf4700de5326f52c5398a8bb94d372ee32663f39b82eb5f84f3350dfc9',3,1,'MyApp','[]',0,'2021-12-03 18:48:59','2021-12-03 18:48:59','2022-12-03 18:48:59'),('50c1068cd8f1bfdc1f070d4d3e2ca52f128067a0aaefa03eaa03f91bf487b426c939689a510b0307',3,1,'MyApp','[]',0,'2021-12-03 07:45:30','2021-12-03 07:45:30','2022-12-03 07:45:30'),('5540ea3e992f4d9a37f8cc82099450f2598ff5b7a6c6432fe0e2f2d06d9c204ba13b24e53fa50725',3,1,'MyApp','[]',0,'2021-12-03 19:05:13','2021-12-03 19:05:13','2022-12-03 19:05:13'),('5abfb4a9705fcd55ea26e55af8cf87e32fc59907ea0f51617ff5852e171dbf8605f1299615d20e45',3,1,'MyApp','[]',0,'2021-12-03 07:41:11','2021-12-03 07:41:11','2022-12-03 07:41:11'),('61b517c630489b9a2b8df11b70fd0a701daa00d7cbd7d87ff45f57139825db78bf89bd607737e402',3,1,'MyApp','[]',0,'2021-11-26 08:34:20','2021-11-26 08:34:20','2022-11-26 08:34:20'),('641d4420d386d6d8d7b0c7abfb88587850281809d03ee40de5f0d9c5bb1e15fc8a808453ddcf0bfa',3,1,'MyApp','[]',0,'2021-12-03 07:40:00','2021-12-03 07:40:00','2022-12-03 07:40:00'),('65de0a09a5f4a9134a1db6bf8d05443f7ca97a3c6f40f48f90b2d185aef6c9025332d619160fac23',5,1,'MyApp','[]',0,'2021-11-26 09:11:59','2021-11-26 09:11:59','2022-11-26 09:11:59'),('6a1c9baf7907fb88bece2874b4c047858f1c862f5bb66d7f17e9a663ac3ec53c5a8068f2c9a5ff62',4,1,'MyApp','[]',0,'2021-11-26 09:11:48','2021-11-26 09:11:48','2022-11-26 09:11:48'),('6a824ba60f503c3a26b1729ffc7ad60444fff42bbeae48a3ee01392ff7e0c1d1d08c382680a58a6d',6,1,'MyApp','[]',0,'2021-11-26 09:12:09','2021-11-26 09:12:09','2022-11-26 09:12:09'),('702a76267ff967f03b58877e091906fc751d9f8ca7351497c45dba4a428b7a79dc689fbbaa1a9118',3,1,'MyApp','[]',0,'2021-12-03 08:24:41','2021-12-03 08:24:41','2022-12-03 08:24:41'),('702f5107a7fb18fe33c0acd5e78bfb8083442798a2c4d46b429f948367b9e7d74530398b957f71e6',3,1,'MyApp','[]',0,'2021-12-03 09:37:30','2021-12-03 09:37:30','2022-12-03 09:37:30'),('708dd705bd8159dbd378f02f8dfa0bde9a503003ca08da69433bf6b781c1d9414636f2a67be7923a',3,1,'MyApp','[]',0,'2021-12-04 07:05:23','2021-12-04 07:05:23','2022-12-04 07:05:23'),('72511f1703db21c32ae52e81553359a626f1ce2e904d28e00011397c2215f2517c0bbf73e33bdef7',9,1,'MyApp','[]',0,'2021-11-26 09:12:39','2021-11-26 09:12:39','2022-11-26 09:12:39'),('73ef1b8f320888c9c37deb6f84e612b38f2d1f2f8ae9429958cd088f01085a3b5795f298f58a8901',3,1,'MyApp','[]',0,'2021-12-03 16:08:58','2021-12-03 16:08:58','2022-12-03 16:08:58'),('7b8f2339330cff8de5b9a4bdecfbf70a0e753b639df47b152e02be2ebaa76bb0ef3895efe69a8150',3,1,'MyApp','[]',0,'2021-12-03 07:59:18','2021-12-03 07:59:18','2022-12-03 07:59:18'),('80c10863fb9a04b23e18be85d25e53ec301e13d80ce52738782250bcfa4508ebe7b54674348db66b',3,1,'MyApp','[]',0,'2021-11-26 10:12:46','2021-11-26 10:12:46','2022-11-26 10:12:46'),('83d3b31d489d4c1d3cfa0c4da9e8da847b21d23ecc51e87e1c5797daf0887678ea3398f3ee1edf4e',3,1,'MyApp','[]',0,'2021-12-03 08:09:20','2021-12-03 08:09:20','2022-12-03 08:09:20'),('84b300403197960bfcc907bdeb89d1a046610364bc4a22e96421b20d5e780831c58a6646ac40570e',3,1,'MyApp','[]',0,'2021-12-03 08:05:48','2021-12-03 08:05:48','2022-12-03 08:05:48'),('866bc766e5e128b90885ce1ebe9778feb6fd18bad27e862636a6bf33ce03b1e8dc36f5a478e0a4ad',3,1,'MyApp','[]',0,'2021-12-03 09:37:09','2021-12-03 09:37:09','2022-12-03 09:37:09'),('88b2c635a6e43e91c601d7db5c2ed7ed06c20ceb727f85c841d7ed16ae1b0394a898baedbe870aec',3,1,'MyApp','[]',0,'2021-12-03 07:42:13','2021-12-03 07:42:13','2022-12-03 07:42:13'),('992af289adf417845f8df3bc1e7f9131684106799393c93d1e07903f89b5ce822e63b88ca810100a',3,1,'MyApp','[]',0,'2021-12-03 16:54:46','2021-12-03 16:54:46','2022-12-03 16:54:46'),('ab988c6ea2b54c494eb2f54837eada43746ce6794bd73d0e593b1fef35dce574c709cb33bc8c87c4',3,1,'MyApp','[]',0,'2021-12-03 07:39:35','2021-12-03 07:39:35','2022-12-03 07:39:35'),('ac36ce384dcc4eae88c74c5e537ce0a6cc492b9e491d5b9de23467599178dcf7f8a254360ec698b1',3,1,'MyApp','[]',0,'2021-12-03 19:03:18','2021-12-03 19:03:18','2022-12-03 19:03:18'),('ac67a90c533ff000425553f5c163d8ea5904edf0c611de84c31468c21f0427a4c38f8f8014b501cd',3,1,'MyApp','[]',0,'2021-12-03 07:44:19','2021-12-03 07:44:19','2022-12-03 07:44:19'),('ac732662749874fe9094324c9c2b410701180e01aa0d474bdb5763882d4071a4655136864ef87542',3,1,'MyApp','[]',0,'2021-12-03 07:44:00','2021-12-03 07:44:00','2022-12-03 07:44:00'),('afae256347f2eda95e5426555e7bf8425230aa4ff13574109311a7a29fae4c0ce8c43cab3fdab721',3,1,'MyApp','[]',0,'2021-12-03 07:40:19','2021-12-03 07:40:19','2022-12-03 07:40:19'),('b2b938e4b7aa8c3d337fb0122bd73eaf3b334f3f5bf3671208d686c8dec418ca000c6ac891b2c433',3,1,'MyApp','[]',0,'2021-12-03 16:54:53','2021-12-03 16:54:53','2022-12-03 16:54:53'),('b3a12e2285a74c6b70b4d725901dd799b0c87ad041b0fdda235f2e79af5387386094f5556f8077d1',3,1,'MyApp','[]',0,'2021-12-03 21:10:43','2021-12-03 21:10:43','2022-12-03 21:10:43'),('b75b921ab0572c67c738e00d5cd81c07a34a71770f367fdbd026a32e248e2eefeaf36856251bc6a9',3,1,'MyApp','[]',0,'2021-11-26 08:34:06','2021-11-26 08:34:06','2022-11-26 08:34:06'),('b883a6faa0875966b476f2ea332489d47308a16702e7366a61ea8851fcef84c8ca39658278141784',3,1,'MyApp','[]',0,'2021-11-26 08:14:37','2021-11-26 08:14:37','2022-11-26 08:14:37'),('b8f05666e0439197c88d43541ade691be7478ce98ec1bc812504ddd110ab92abde14ad525aa4e687',3,1,'MyApp','[]',0,'2021-12-03 07:59:36','2021-12-03 07:59:36','2022-12-03 07:59:36'),('baff77b876ef72cf7e5e3b358f526eb2da08c85008d6ce7d6f85fd40e5733b8bd21a09a036657864',3,1,'MyApp','[]',0,'2021-12-01 15:15:46','2021-12-01 15:15:46','2022-12-01 15:15:46'),('bcb064e22d32c3169ff7edc0f54771242c1ed352f0da3e4dcf4ec8d4ac20724b6d8ec0d7aab982bb',3,1,'MyApp','[]',0,'2021-12-03 19:13:55','2021-12-03 19:13:55','2022-12-03 19:13:55'),('be90e5a3f172c8fee9f930ccdb2769973d05eaeb96f17667391b02c68fe12d39092190a150c0705d',3,1,'MyApp','[]',0,'2021-11-26 08:40:46','2021-11-26 08:40:46','2022-11-26 08:40:46'),('bf08c97d9d813c89be56f4e7dd397efd2c09e3b1111d00a75fbd1e269104bfd173c1cb5e9c414a0a',3,1,'MyApp','[]',0,'2021-12-03 08:10:05','2021-12-03 08:10:05','2022-12-03 08:10:05'),('c96436e55bd3ef4d2506aec4702158b61bfba6df9b0de3d4ef0a72a0d475ab039186d8943903ab1a',10,1,'MyApp','[]',0,'2021-11-26 09:12:46','2021-11-26 09:12:46','2022-11-26 09:12:46'),('cfe63d7c81453a0f26c2f27f209f320a0396acf20e13b2c3acf4b6aa157494fe83d7101ca3ee1e0e',8,1,'MyApp','[]',0,'2021-11-26 09:12:28','2021-11-26 09:12:28','2022-11-26 09:12:28'),('d159893a7912c7d06bd8f3ae21c40c0ec1cd1f1beb6e0c4327630f1f9f0d0bb09ee7555319e4f2e1',3,1,'MyApp','[]',0,'2021-12-03 07:51:58','2021-12-03 07:51:58','2022-12-03 07:51:58'),('d2a9b74fc112d98272f8b5d32050fb13c3c5399effb454483c9d9cd8d38dccc4ba72ca8d7a8b31d1',3,1,'MyApp','[]',0,'2021-12-03 19:14:20','2021-12-03 19:14:20','2022-12-03 19:14:20'),('d35de95283a14b919e4c54998488cd6408e1fb6bc906bf8d291630d6c7db25dd2a4beafa4c1d37b6',3,1,'MyApp','[]',0,'2021-11-26 08:15:06','2021-11-26 08:15:06','2022-11-26 08:15:06'),('d5b6af66ae13c1d9b8b31292fca6d084cd88a8230b38d67d631854530c30bd28d240a5af86fafbe7',3,1,'MyApp','[]',0,'2021-12-03 16:54:49','2021-12-03 16:54:49','2022-12-03 16:54:49'),('d9c8577331b5ec1fed5c29d6c55a502e2b3af09a99a3b1ed94a8e632381b148cd134fac23d0a022f',3,1,'MyApp','[]',0,'2021-12-03 19:01:30','2021-12-03 19:01:30','2022-12-03 19:01:30'),('ea5ae33e3cad3530fddf104fa5b54067d4677dcc7e4ea4c7d8a03a87f9f62c4219bc3c77f7020768',3,1,'MyApp','[]',0,'2021-11-26 10:11:59','2021-11-26 10:11:59','2022-11-26 10:11:59'),('ef1319346242f70362bb12db2af97e01b7d4d56e87179381753ad50d6040e50b2340f3986d304ef7',3,1,'MyApp','[]',0,'2021-12-03 08:18:05','2021-12-03 08:18:05','2022-12-03 08:18:05'),('fa0f0b6985c80c639e1bdabe3b51c5402e029a67c3981f762445ad3f6109c6d0acc84f744270fbed',3,1,'MyApp','[]',0,'2021-12-03 20:23:23','2021-12-03 20:23:23','2022-12-03 20:23:23'),('ff4148b7e58fcb8069bef96540cb18931246bd12cc9284c9d249741a095c2e45db72e6671be7990b',7,1,'MyApp','[]',0,'2021-11-26 09:12:16','2021-11-26 09:12:16','2022-11-26 09:12:16');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','FPjGkeIhwRtTAMo48zpRgYpaEDjkwnfauhTM9lMU',NULL,'http://localhost',1,0,0,'2021-11-26 08:14:20','2021-11-26 08:14:20'),(2,NULL,'Laravel Password Grant Client','da5q9fYzTRX2tgdPSfHtvBmb9ePSU43e4pFtQJWS','users','http://localhost',0,1,0,'2021-11-26 08:14:20','2021-11-26 08:14:20');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2021-11-26 08:14:20','2021-11-26 08:14:20');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_positions`
--

DROP TABLE IF EXISTS `user_positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_positions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_positions`
--

LOCK TABLES `user_positions` WRITE;
/*!40000 ALTER TABLE `user_positions` DISABLE KEYS */;
INSERT INTO `user_positions` VALUES (1,'Director',NULL,NULL),(2,'Manager',NULL,NULL),(3,'Developer',NULL,NULL),(4,'QC',NULL,NULL),(5,'Sales Executive',NULL,NULL);
/*!40000 ALTER TABLE `user_positions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'Manager',NULL,NULL),(2,'Employee',NULL,NULL);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '1',
  `position_id` bigint(20) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  KEY `users_position_id_foreign` (`position_id`),
  CONSTRAINT `users_position_id_foreign` FOREIGN KEY (`position_id`) REFERENCES `user_positions` (`id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'Phuc Nguyen','phucnguyen.snow@gmail.com',NULL,'$2y$10$euNQo8Hg.qgU/c4w5UuW3.Six7JAGq27Jfv9yi19eU.gdS.Wfvseq',NULL,'2021-11-26 08:14:37','2021-11-26 08:14:37',1,1),(4,'Mark','mark@gmail.com',NULL,'$2y$10$UB8kqKRknZdqPRMuw6UfUOecsQC/DSMD1DfUtLECIs18.QstMJKfq',NULL,'2021-11-26 09:11:48','2021-11-26 09:11:48',2,2),(5,'Jake','jake@gmail.com',NULL,'$2y$10$vL/LC4HdPHpoStPxITxmI.1Q20HMZlCY6XCIpI8zJyCYtfcQSxZ12',NULL,'2021-11-26 09:11:59','2021-11-26 09:11:59',2,2),(6,'Kylie','kylie@gmail.com',NULL,'$2y$10$Z/2gJAns15IM5v5aLeNsPOWUdvSqGOCyoi8Q.chT6.0zHsQLK4vgS',NULL,'2021-11-26 09:12:09','2021-11-26 09:12:09',2,2),(7,'Vince','vince@gmail.com',NULL,'$2y$10$/9glQfU5SMiJ7bA7ZPa3QeL6jRfeY60y1Ud64cegS6Io4.4iieIjq',NULL,'2021-11-26 09:12:16','2021-11-26 09:12:16',2,2),(8,'Ostra','ostra@gmail.com',NULL,'$2y$10$RwBf38MxHHIKk1rlVqKYz.4CVCX7WDNqC7KjQQuq5RqvJS6..HKqa',NULL,'2021-11-26 09:12:28','2021-11-26 09:12:28',2,2),(9,'Phil','phil@gmail.com',NULL,'$2y$10$wlYG1JWXvIQAgqmDg1OWk.e71eI.d632B.CsnuOF.//SZYARMyvMe',NULL,'2021-11-26 09:12:39','2021-11-26 09:12:39',2,2),(10,'Robb','robb@gmail.com',NULL,'$2y$10$Spyta8.XN3bL77QQaqvV7exMZ5hB.lRaUjMDrhH4BWMgdXkGRjcYy',NULL,'2021-11-26 09:12:46','2021-11-26 09:12:46',2,2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-05  8:05:56
