<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'App\Http\Controllers\Api\UserController@login');
Route::post('register', 'App\Http\Controllers\Api\UserController@register');
/**
 * Wrap api routes in this group to authorize
 */
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('user', 'App\Http\Controllers\Api\UserController@index');
    Route::post('details', 'App\Http\Controllers\Api\UserController@details');
    Route::post('user/setRole', 'App\Http\Controllers\Api\UserController@setRole');
    Route::post('user/assets', 'App\Http\Controllers\Api\UserController@getAssets');

    /**
     * Resource: Leave Request
     */
    Route::post('leave-request', 'App\Http\Controllers\Api\LeaveRequestController@index')->name('leave.request.list');
    Route::post('leave-request/filter', 'App\Http\Controllers\Api\LeaveRequestController@filter')->name('leave.request.filter');
    Route::post('leave-request/add', 'App\Http\Controllers\Api\LeaveRequestController@store')->name('leave.request.create');
    Route::put('leave-request', 'App\Http\Controllers\Api\LeaveRequestController@update')->name('leave.request.update');
    Route::patch('leave-request', 'App\Http\Controllers\Api\LeaveRequestController@updatePartial')->name('leave.request.updatePartial');
    Route::delete('leave-request', 'App\Http\Controllers\Api\LeaveRequestController@destroy')->name('leave.request.delete');
    Route::options('leave-request', 'App\Http\Controllers\Api\LeaveRequestController@getAssets')->name('leave.request.assets');
});
